CategoryMember
==============
```{{#ifcatmember:<Category>|<Then>|<Else>|<Page>}}```

Uses the actual page if `<Page>` is omitted.

