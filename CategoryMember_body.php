<?php

if (!defined('MEDIAWIKI')) {
    echo "This file is an extension of the MediaWiki software and cannot be used standalone\n";
    die(1);
}

class CategoryMember {

    /**
     * Parser-hook
     *
     * @param Parser $parser
     */
    static function onParserFirstCallInit($parser) {
        $parser->setFunctionHook('ifcatmember', 'CategoryMember::onIfCatMember');
    }

    /**
     *
     * @param Category $category
     * @param WikiPage $wikiPage
     */
    static function onCategoryAdded(Category $category, WikiPage $wikiPage ) {
        $wikiPage->getTitle()->invalidateCache();
    }

    /**
     *
     * @param Category $category
     * @param WikiPage $wikiPage
     */
    static function onCategoryRemoved(Category $category, WikiPage $wikiPage ) {
        $wikiPage->getTitle()->invalidateCache();
    }

    /**
     * Main parser-function
     *
     * @param Parser $parser
     * @param string $category
     * @param string $then
     * @param string $else
     * @param string $pagename
     * @return string
     */
    static function onIfCatMember(Parser $parser, $category, $then, $else, $pagename = '') {
        //$parser->disableCache();
        if ($category === '') {
            return $else;
        }

        if ($pagename === '') {
            $title = $parser->getTitle();
            $page = new WikiPage($title);
            $ns = $title->getNamespace();
        } else {
            $title = Title::newFromText($pagename);
            if (!( $title instanceOf Title ) || !$title->exists()) {
                return $else;
            }
            $page = new WikiPage($title);
            $ns = $title->getNamespace();
        }

        $cattitle = Title::makeTitleSafe(NS_CATEGORY, $category);
        if (!( $cattitle instanceOf Title )) {
            return $else;
        }

        $catkey = $cattitle->getDBkey();

        $wgExtCategoryMember = new CategoryMember();
        if ($wgExtCategoryMember->isPageInCatOrSubCat($catkey, $page, $ns, $title)) {
            $ret = $then;
        } else {
            $ret = $else;
        }

        return array($ret, 'noparse' => false );
    }

    /*
     * Member-functions
     *
     */

    /**
     *
     * @param type $catkey
     * @param WikiPage $page
     * @param string $ns
     * @param Title $title
     * @return bool
     */
    private function isPageInCatOrSubCat($catkey, WikiPage $page, $ns, Title &$title) {
        $catids = array_values(iterator_to_array($page->getCategories(), false));
        $catids = array_map(function($v) {return $v->getDBkey();}, $catids);
        $catids = array_merge($catids, $this->getParentCats($catids));

        return (in_array($catkey, $catids));
    }

    /**
     *
     * @param array $cats
     * @param array $found
     * @return array
     */
    private function getParentCats($cats, $found = []) {
        $dbr = wfGetDB(DB_REPLICA);

        foreach ($cats as $cat) {
            $res = $dbr->select(
                    array('p' => 'page', 'cl' => 'categorylinks'), array('cl_to'), array(
                'page_id=cl.cl_from',
                'cl_type' => 'subcat',
                'page_title' => $cat,
                'page_namespace' => 14
                    ), __METHOD__, ''
            );

            $ps = array();
            while ($row = $res->fetchRow()) {
                if (!in_array($row['cl_to'], $found)) {
                    $ps[] = $row['cl_to'];
                }
            }

            $found = array_unique(array_merge($found, $ps));
            if (!empty($ps)) {
                $found = array_unique(array_merge($found, $this->getParentCats($ps, $found)));
            }
        }

        return $found;
    }
}
